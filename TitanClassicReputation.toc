## Title: Titan Classic [|cffeda55fReputation|r]
## Authors: Yunohu
## Notes: Adds Reputation tracking and monitoring tools to Titan Panel.
## Version: 1.0.0.1
## Interface: 11305
## Dependencies: TitanClassic
## OptionalDeps: Glamour
## SavedVariables: TitanRep_Data
TitanClassicReputation.xml
